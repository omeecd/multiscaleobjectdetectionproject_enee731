% trivial feature extractor: compute mean RGB
function fd = extractfdTrain(VOCopts,I, bb)
TrainImg=imcrop(I, bb);
TrainImg=reshape(TrainImg, [64,64]);
fd=squeeze(sum(sum(double(TrainImg)))/(size(TrainImg,1)*size(TrainImg,2)));
