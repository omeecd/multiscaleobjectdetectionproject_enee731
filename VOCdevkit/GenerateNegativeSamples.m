%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is the main code for training SVM classifier and Hard 
% Negative Data mining on PASCAL VOC 2007 dataset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed By:
% Upal Mahbub, Graduate Student, UMD, 2014
% UID: 113101908
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
clear all;
close all;


% change this path if you install the VOC code elsewhere
addpath('./VOCcode/');

% initialize VOC options
VOCinit;
WinSize=128;
plotEn=0;
NClass=20;
% % train and test detector for each class
NClass=1; %VOCopts.nclasses;
count=0;
for k=15% 1:1 for aeroplane; 7:7 for cars; 1:NClass for all classes
    cls=VOCopts.classes{k};
    
    %%%%%%%%%%%%% Read the Images and Crop %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % load 'train' image set
    ids=textread(sprintf(VOCopts.imgsetpath,'train'),'%s');
    % extract features and bounding boxes   
    
    Nhard=100; %%% Number of samples to be taken from each image for hard negative data mining
    for i=1:length(ids)
        % display progress
        if toc>1
            fprintf('%s: Hard Negative Data Mining: %d/%d\n',cls,i,length(ids));
            drawnow;
            tic;
        end
        
        % read annotation
        rec=PASreadrecord(sprintf(VOCopts.annopath,ids{i}));
        
        % find objects of class and WinSize(SizeN)extract difficult flags for these objects
        clsinds=strmatch(cls,{rec.objects(:).class},'exact');
        diff=[rec.objects(clsinds).difficult];
        
        % assign ground truth class to image
        if isempty(clsinds)
            gt=-1;          % no objects of class
        elseif any(~diff)
            gt=1;           % at least one non-difficult object of class
        else
            gt=0;           % only difficult objects
        end
        
        if gt==-1
            I=imread(sprintf(VOCopts.imgpath,ids{i}));
            I=rgb2gray(I);
%             I=imresize(I, [512 512]);
            IndX=randperm(size(I,1)-WinSize, Nhard);
            IndY=randperm(size(I,2)-WinSize, Nhard);
            
            for j=1:length(IndX)
                NNrect=[IndY(j), IndX(j), WinSize, WinSize];
                TrainImgNeg=double(imcrop(I, NNrect));
                imwrite(TrainImgNeg, ['C:/PrepareForGoogle/DPM_Based_Face_Detection_Upal/HaarCascadeTrainUpal/negative_images/' count '.jpg']);
                count=count+1;
            end
        end
    end
end
