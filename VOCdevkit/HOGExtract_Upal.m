function [hog]=HOGExtract_Upal(Image, plotEn, params)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function takes the rgb image and a parameter list (param) as
% input and returns the following features:
% 1. HOGImg: Histogram of Gradient image of depth image (requeres VL_Feat toolbox available at http://www.vlfeat.org/ 
% If plotEn = 1, the outputs are plotted in different figures;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code Written By:
% Upal Mahbub, Graduate Student, UMD, 2014
% UID: 113101908
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<3
    params.LBP=[-1 -1; -1 0; -1 1; 0 -1; -0 1; 1 -1; 1 0; 1 1];
    params.GradType='Sobel';
    param.HOGcellSize = 4;
    param.HOGNOrient=9;
    Param.smoothingFilter = fspecial('gaussian',[10 10], 5);
end
if nargin<2
    plotEn = 0;
end

%%%%% HOG Extraction %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hog = vl_hog(im2single(Image), param.HOGcellSize, 'numOrientations', param.HOGNOrient);


if plotEn
    HOGImg = vl_hog('render', hog, 'numOrientations', param.HOGNOrient);
    figure;
    subplot(1, 2, 1), imagesc(Image); colormap(gray); title('Grayscale Image');
    subplot(1, 2, 2), imagesc(HOGImg) ; colormap gray ; title('HOG Representation of Image');
end
