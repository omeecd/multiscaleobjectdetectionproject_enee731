%%%% ENEE 731: Image Understanding %%%%%%%%%%%
Project 1: Object Detection from images

Submitted by:
Upal Mahbub, 
UID: 113101908, 
University of Maryland, College Park 
2014.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

1. Please unzip the 'VOCtrainval_06-Nov-2007' (extract as the file name with the same name ('VOCtrainval_06-Nov-2007')) to this folder ('finalSubmissionProj1'). That is, you after unzipping
you will have two folders:
	a. VOCdevkit (already provided)
	b. VOCtrainval_06-Nov-2007 (note: the folder immediately inside this is also named VOCdevkit)
2. run ./VOCdevkit/Upal_TrainClasses_HardNegMining to train a SVM classifier
and improve by hard negative data mining.
	The variable k denotes the class ID (can be 1 to 20)
	The variable i denotes the image index
This function saves the model with the name <class-name>-model.mat in ./VOCdevkit/local/VOC2007/

	
3. run ./VOCdevkit/Upal_ObjectDetection_main to perform object detection on
test images. Again:
	The variable k denotes the class ID (can be 1 to 20)
	The variable i denotes the image index
	Also,
	the address for ids can be changed to use different test sets.
	if plotEn is set, the images and bounding boxes will be shown.
	The results are saved in ./VOCdevkit/results/VOC2007/Main/comp3upal_det_val_<class-name>.txt
	
The HOGExtract_Upal file extracts the HOG features. It requires the vl_feat library (provided in the /VOCdevkit folder. Also, for training SVM the liblinear toolbox is used which is also provided in the ./VOCdevkit folder.
