%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is the main code for training SVM classifier and Hard 
% Negative Data mining on PASCAL VOC 2007 dataset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed By:
% Upal Mahbub, Graduate Student, UMD, 2014
% UID: 113101908
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
clear all;
close all;


% change this path if you install the VOC code elsewhere
addpath('./VOCcode/');
addpath(genpath('./liblinear-1.95/'));
addpath(genpath('./vlfeat-0.9.18-bin/'));

% initialize VOC options
VOCinit;
WinSize=128;
plotEn=0;
NClass=20;
% % train and test detector for each class
NClass=1; %VOCopts.nclasses;
for k=7:7% 1:1 for aeroplane; 7:7 for cars; 1:NClass for all classes
    cls=VOCopts.classes{k};
    
    %%%%%%%%%%%%% Read the Images and Crop %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % load 'train' image set
    ids=textread(sprintf(VOCopts.imgsetpath,'train'),'%s');
    % extract features and bounding boxes
    tic;
    TrainMat{1}=[];TrainMat{2}=[];
    TrainLabel{1}=[];TrainLabel{2}=[];
    
    for i=1:length(ids)
        % display progress
        if toc>1
            fprintf('%s: train: %d/%d\n',cls,i,length(ids));
            drawnow;
            tic;
        end
        
        % read annotation
        rec=PASreadrecord(sprintf(VOCopts.annopath,ids{i}));
        
        % find objects of class and WinSize(SizeN)extract difficult flags for these objects
        clsinds=strmatch(cls,{rec.objects(:).class},'exact');
        diff=[rec.objects(clsinds).difficult];
        
        % assign ground truth class to image
        if isempty(clsinds)
            gt=-1;          % no objects of class
        elseif any(~diff)
            gt=1;           % at least one non-difficult object of class
        else
            gt=0;           % only difficult objects
        end
        
        if gt==1
            I=imread(sprintf(VOCopts.imgpath,ids{i}));
            TrainImg=[];
            for jj=1:length(clsinds)
                boxCoord=rec.objects(clsinds(jj)).bbox;
                bbox=[boxCoord(1) boxCoord(2) boxCoord(3)-boxCoord(1) boxCoord(4)-boxCoord(2)];
                TrainImg=(imcrop(I, bbox));
                if(plotEn)
                    figure(1); subplot(141), imshow(uint8(TrainImg));
                end
                TrainImg=double(rgb2gray(TrainImg));
                
                if(plotEn)
                    figure(1); subplot(142), imshow(uint8(I));
                end
                
                if(plotEn)
                    figure(1); subplot(143), imshow(uint8(TrainImg));
                end
                SizedImg=[];
                SizedImg=imresize(TrainImg, [WinSize,WinSize]);
                if(plotEn)
                    figure(1); subplot(144),  imshow(uint8(SizedImg));
                end
                for mm=1:2
                    for nn=1:2
                        %%% Training partial SVM classifier
                        temp=SizedImg((mm-1)*WinSize/2+1:(mm)*WinSize/2,(nn-1)*WinSize/2+1:(nn)*WinSize/2);
                        [HOGImg]=HOGExtract_Upal(temp, 0);
                        TrainMat{2}=[TrainMat{2} [HOGImg(:)]];
                        TrainLabel{2}=[TrainLabel{2}; 1];
                    end
                end
                temp=imresize(SizedImg, [WinSize/2, WinSize/2]);
                HOGImg=HOGExtract_Upal(temp, 0);
                TrainMat{1}=[TrainMat{1} HOGImg(:)];
                TrainLabel{1}=[TrainLabel{1}; 1];
            end
            
            PosLoc=[];
            for jj=1:length(clsinds)
                x1=1:size(I, 2)-128;
                y1=1:size(I, 1)-128;
                [XI, YI]=meshgrid(x1, y1);
                bbox=rec.objects(clsinds(jj)).bbox;
                
                xpos=max(bbox(1)-128,0):bbox(3);
                ypos=max(bbox(2)-128,0):bbox(4);
                [XIPos, YIPos]=meshgrid(xpos, ypos);
                PosLoc=[PosLoc; XIPos(:) YIPos(:)];
            end
            
            zz=setdiff([XI(:) YI(:)], PosLoc, 'rows');
            
            if(~isempty(zz))
                IndXY=randperm(size(zz,1), 5);
                
                for j=1:length(IndXY)
                    NNrect=[zz(IndXY(j),1), zz(IndXY(j), 2), WinSize, WinSize];
                    TrainImgNeg=(imcrop(I, NNrect));
                    TrainImgNeg=double(rgb2gray(TrainImgNeg));
                    
                    SizedImgNeg=imresize(TrainImgNeg, [WinSize,WinSize]);
                    
                    temp=imresize(SizedImgNeg, [WinSize/2, WinSize/2]);
                    HOGImg=HOGExtract_Upal(temp, 0);
                    TrainMat{1}=[TrainMat{1} HOGImg(:)];
                    TrainLabel{1}=[TrainLabel{1}; 0];
                    TrainMat{2}=[TrainMat{2} HOGImg(:)];
                    TrainLabel{2}=[TrainLabel{2}; 0];
                end
            end
        end
    end
    
    fprintf('Training SVM\n');
    model{1} = train(double(TrainLabel{1}), sparse(double(TrainMat{1})'), '-s 2');
    model{2} = train(double(TrainLabel{2}), sparse(double(TrainMat{2})'), '-s 2');
    
    Nhard=20; %%% Number of samples to be taken from each image for hard negative data mining
    for i=1:length(ids)
        % display progress
        if toc>1
            fprintf('%s: Hard Negative Data Mining: %d/%d\n',cls,i,length(ids));
            drawnow;
            tic;
        end
        
        % read annotation
        rec=PASreadrecord(sprintf(VOCopts.annopath,ids{i}));
        
        % find objects of class and WinSize(SizeN)extract difficult flags for these objects
        clsinds=strmatch(cls,{rec.objects(:).class},'exact');
        diff=[rec.objects(clsinds).difficult];
        
        % assign ground truth class to image
        if isempty(clsinds)
            gt=-1;          % no objects of class
        elseif any(~diff)
            gt=1;           % at least one non-difficult object of class
        else
            gt=0;           % only difficult objects
        end
        
        if gt==-1
            I=imread(sprintf(VOCopts.imgpath,ids{i}));
            I=rgb2gray(I);
            I=imresize(I, [512 512]);
            IndX=randperm(size(I,1)-WinSize, Nhard);
            IndY=randperm(size(I,2)-WinSize, Nhard);
            count=0;
            for j=1:length(IndX)
                NNrect=[IndY(j), IndX(j), WinSize, WinSize];
                TrainImgNeg=double(imcrop(I, NNrect));
                %                 figure(1); subplot(121), imshow(uint8(TrainImgNeg));
                
                temp=imresize(TrainImgNeg, [WinSize/2, WinSize/2]);
                HOGImg=HOGExtract_Upal(temp, 0);
                TestFeat=HOGImg(:);
                [Group, accuracy, ConfMat] = predict(0, sparse(double(TestFeat')), model{1}); % run the SVM model on the test data
                if (Group==1)
                    TrainMat{1}=[TrainMat{1} TestFeat];
                    TrainLabel{1}=[TrainLabel{1}; 0];
                end
                if(count<5)
                    [Group, accuracy, ConfMat] = predict(0, sparse(double(TestFeat')), model{2}); % run the SVM model on the test data
                    if (Group==1)
                        TrainMat{2}=[TrainMat{2} TestFeat];
                        TrainLabel{2}=[TrainLabel{2}; 0];
                    end
                    count=count+1;
                end
            end
        end
    end
    fprintf('Re-Training SVM\n');
    model{1} = train(double(TrainLabel{1}), sparse(double(TrainMat{1}')), '-s 2');
    model{2} = train(double(TrainLabel{2}), sparse(double(TrainMat{2}')), '-s 2');
    
    save(sprintf(strrep(VOCopts.exTrainpath, 'Train.mat', '_Model.mat'),cls),'model', '-v7.3');
end
