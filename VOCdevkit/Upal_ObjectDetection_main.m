%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is the main code for object detection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Developed By:
% Upal Mahbub, Graduate Student, UMD, 2014
% UID: 113101908
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clc;
clear all;
close all;


% change this path if you install the VOC code elsewhere
addpath('./VOCcode/');
addpath(genpath('./liblinear-1.95/'));
addpath(genpath('./vlfeat-0.9.18-bin/'));
% initialize VOC options
VOCinit;
WinSize=[128];

NClass=20; %VOCopts.nclasses;

%%%%%%%%%%%%% Test %%%%%%%%%%%%%%%%%%%%%%%%%%%%
WinSize=[512 256 128 64];

plotEn=1;
Nfact=[8 16 4 4];
flag=0;
for k=1:1 % 1:1 for aeroplane; 7:7 for cars; 1:NClass for all classes
    cls=VOCopts.classes{k};
    [ids,gt]=textread(sprintf(VOCopts.imgsetpath,[cls '_val']),'%s %d');
    fid=fopen(sprintf(VOCopts.detrespath,'comp3upal',cls),'w');
    for i=1:length(ids)
        Iin=(imread(sprintf(VOCopts.imgpath,ids{i})));
        Itest1=rgb2gray(Iin);
        Itest=imresize(Itest1, [WinSize(1) WinSize(1)]);
        if flag==0
            OlapImg=zeros(size(Itest));
        end
        coefTest=[];
        TestMat{1}=[];TestMat{2}=[];TestMat{3}=[];TestMat{4}=[];TestMat{5}=[];
        for SizeN=1:length(WinSize)
            RatioFact(SizeN, :)=size(Itest)./size(Itest1);
            
            fprintf('Handling Window of Size  %d\n', WinSize(SizeN));
            for j=1:WinSize(SizeN)/Nfact(SizeN):size(Itest,1)-WinSize(SizeN)+1
                for l=1:WinSize(SizeN)/Nfact(SizeN):size(Itest,2)-WinSize(SizeN)+1
                    if(WinSize(SizeN)==64 && flag==0)
                        OlapImg(j:j+64-1, l:l+64-1)=OlapImg(j:j+64-1, l:l+64-1)+1;
                    end
                    Iwin=[];
                    Iwin=(Itest(j:j+WinSize(SizeN)-1, l:l+WinSize(SizeN)-1));
                    Iwin=double((Iwin));
                    if (WinSize(SizeN)~=64)
                        Iwin=imresize(Iwin, [64, 64]);
                    end
                    HOGImg=HOGExtract_Upal(Iwin, 0);
                    %[HistVal xx]=imhist(Iwin/max(Iwin(:)));
                    TestMat{SizeN}=[TestMat{SizeN} [HOGImg(:)]];
                end
            end
            if(WinSize(SizeN)==64 && flag==0)
                flag=1;
            end
        end
        OlapImg=imresize(OlapImg, size(Itest1));
        fprintf('Processing Test Image : %d\n', i);
        if plotEn
            %clf;
            figure; imshow(Itest1);
        end
        
        load(sprintf(strrep(VOCopts.exTrainpath, 'Train.mat', '_Model.mat'),cls));
        
        
        for SizeN=1:length(WinSize)
            testing_label_vector=double(zeros(size(TestMat{SizeN}, 2), 1));
            [Group{i, k, SizeN}, accuracy, ConfMat{i, k, SizeN}] = predict(testing_label_vector, sparse(double(TestMat{SizeN}')), model{1}); % run the SVM model on the test data
        end
        [Group2{i, k}, accuracy2, ConfMat2{i, k}] = predict(testing_label_vector, sparse(double(TestMat{SizeN}')), model{2}); % run the SVM model on the test data
        
        %%%%%%%%%%%%
        % %         for i=1:length(ids)
        for SizeN=1:length(WinSize)
            x=length(1:WinSize(SizeN)/Nfact(SizeN):size(Itest,1)-WinSize(SizeN)+1);
            y=length(1:WinSize(SizeN)/Nfact(SizeN):size(Itest,2)-WinSize(SizeN)+1);
            
            count=1;
            for j=1:x
                for jj=1:y
                    %                     Z{i, k, SizeN}(jj,j)=ConfMat{i, k, SizeN}(count,1);
                    Grp{i, k, SizeN}(jj,j)=Group{i, k, SizeN}(count);
                    if (SizeN==length(WinSize))
                        Grp2{i, k}(jj,j)=Group2{i, k}(count);
                    end
                    count=count+1;
                end
            end
            %             fprintf('Max Confidence: %f\n', max(max(Z{i,k, SizeN}(Grp{i, k, SizeN}==1))));
            %             [rg512{SizeN} cg512{SizeN}]=find(Grp{i, k, SizeN}==1 & Z{i, k, SizeN}>th{k}(SizeN));
            [rg512{SizeN} cg512{SizeN}]=find(Grp{i, k, SizeN}==1);
            rg1{SizeN}=round((rg512{SizeN}-1)/RatioFact(SizeN, 2).*(WinSize(SizeN)/Nfact(SizeN)))+1;
            cg1{SizeN}=round((cg512{SizeN}-1)/RatioFact(SizeN, 1).*(WinSize(SizeN)/Nfact(SizeN)))+1;
            bboxG1{SizeN}=[rg1{SizeN} cg1{SizeN} rg1{SizeN}+floor(WinSize(SizeN)/RatioFact(SizeN, 2))-1 cg1{SizeN}+floor(WinSize(SizeN)/RatioFact(SizeN, 1))-1];
            %             bboxG1{SizeN}=[(rg1{SizeN}-1).*(WinSize(SizeN)/Nfact/RatioFact(SizeN, 2))+1 (cg1{SizeN}-1).*(WinSize(SizeN)/Nfact/RatioFact(SizeN, 1))+1 (rg1{SizeN}-1).*WinSize(SizeN)/Nfact/RatioFact(SizeN, 2)+1+WinSize(SizeN)/RatioFact(SizeN, 2) (cg1{SizeN}-1).*WinSize(SizeN)/Nfact/RatioFact(SizeN, 1)+1+WinSize(SizeN)/RatioFact(SizeN, 1)];
        end
        
        [rg64 cg64]=find(Grp2{i, k}==1);
        rg2=round((rg64-1)/RatioFact(SizeN, 2).*(WinSize(SizeN)/Nfact(SizeN)))+1;
        cg2=round((cg64-1)/RatioFact(SizeN, 1).*(WinSize(SizeN)/Nfact(SizeN)))+1;
        bboxG2=[rg2 cg2 rg2+floor(WinSize(SizeN)/RatioFact(SizeN, 2))-1 cg2+floor(WinSize(SizeN)/RatioFact(SizeN, 1))-1];
        %             bboxG1{SizeN}=[(rg1{SizeN}-1).*(WinSize(SizeN)/Nfact/RatioFact(SizeN, 2))+1 (cg1{SizeN}-1).*(WinSize(SizeN)/Nfact/RatioFact(SizeN, 1))+1 (rg1{SizeN}-1).*WinSize(SizeN)/Nfact/RatioFact(SizeN, 2)+1+WinSize(SizeN)/RatioFact(SizeN, 2) (cg1{SizeN}-1).*WinSize(SizeN)/Nfact/RatioFact(SizeN, 1)+1+WinSize(SizeN)/RatioFact(SizeN, 1)];
        
        CountImg=zeros(size(Itest1));
        if (~isempty(bboxG2))
            for nn=1:length(bboxG2(:,1))
                for u=bboxG2(nn,1):bboxG2(nn,3)
                    for v=bboxG2(nn,2):bboxG2(nn,4)
                        CountImg(v,u)=CountImg(v,u)+1;
                    end
                end
            end
        end
        CountImg=CountImg./OlapImg;
        th(1,:)=[0.02 0.03 0.07 0.1];
        th(7,:)=[0.03 0.17 0.16 0.2];
        th(15,:)=[0.1 0.12 0.2 0.28];
        ls=[1 1 0; 1 0 1; 0 1 1; 1 1 1];
        colorS={'y' 'm' 'c' 'r' 'b'};
        
        confidence=[];
        confValFin=[];
        bboxFin=[];
        countN=1;
        for SizeN=1:length(WinSize)-1
            if (~isempty(bboxG1{SizeN}))
                for mm=1:length(bboxG1{SizeN}(:,1))
                    temp=imcrop(CountImg, bboxG1{SizeN}(mm,:));
                    confidence(SizeN, mm)=sum(temp(:))/numel(temp);
                    
                    if(confidence(SizeN, mm)>th(k, SizeN))
                        confVal=confidence(SizeN, mm)/max(confidence(SizeN, :));
                        confValFin(countN)=confVal;
                        bboxFin(countN,:)=bboxG1{SizeN}(mm,:);
                        countN=countN+1;
                    end
                end
            end
        end
        
        if(~isempty(confValFin))
            BoxImg=zeros(size(Itest1));
            for nn=1:length(bboxFin(:,1))
                for u=bboxFin(nn,1):bboxFin(nn,3)
                    for v=bboxFin(nn,2):bboxFin(nn,4)
                        BoxImg(v,u)=BoxImg(v,u)+1;
                    end
                end
            end
            
            sumX=[0 sum(BoxImg,1) 0];
            sumY=[0; sum(BoxImg,2); 0];
            
            countN=1;
            Xpos1=[];Xpos2=[]; Ypos1=[]; Ypos2=[];
            
            for mm=2:length(sumX)
                if((sumX(mm-1)==0 && sumX(mm)>0))
                    Xpos1(countN)=mm-1;
                elseif((sumX(mm-1)>0 && sumX(mm)==0))
                    Xpos2(countN)=mm-2;
                    countN=countN+1;
                end
            end
            
            
            countN=1;
            for mm=2:length(sumY)
                if((sumY(mm-1)==0 && sumY(mm)>0))
                    Ypos1(countN)=mm-1;
                elseif((sumY(mm-1)>0 && sumY(mm)==0))
                    Ypos2(countN)=mm-2;
                    countN=countN+1;
                end
            end
            
            if(length(Xpos1)>length(Ypos1))
                for mm=1:length(Xpos1)
                    zz=find(BoxImg(:,Xpos1(mm))>0);
                    Ypos1(mm)=min(zz);
                    Ypos2(mm)=max(zz);
                end
            elseif(length(Ypos1)>length(Xpos1))
                for mm=1:length(Ypos1)
                    zz=find(BoxImg(Ypos1(mm),:)>0);
                    Xpos1(mm)=min(zz);
                    Xpos2(mm)=max(zz);
                end
            end
            
            bboxFinCoord=[];
            bboxFinCoord=[Xpos1(:) Ypos1(:) Xpos2(:) Ypos2(:)];
            confCoord=[];
            for mm=1:length(bboxFinCoord(:,1))
                DistFunc=sqrt(sum((bboxFin(:,1:2)-repmat(bboxFinCoord(mm,1:2),length(bboxFin(:,1)), 1)).^2, 2));
                zz=find((DistFunc==min(DistFunc)));
                confCoord(mm)=confValFin(zz(1));
            end
            
            for mm=1:length(confCoord)
                fprintf('%s %f %d %d %d %d\n',ids{i},confCoord(mm),bboxFinCoord(mm,:))
                fprintf(fid,'%s %f %d %d %d %d\n',ids{i},confCoord(mm),bboxFinCoord(mm,:));
                if plotEn
                    hold on; plot(bboxFinCoord(mm, [1 3 3 1 1]),bboxFinCoord(mm, [2 2 4 4 2]),colorS{SizeN},'linewidth',2);
                    text(bboxFinCoord(mm,1),bboxFinCoord(mm,2),cls,'color','k','backgroundcolor',ls(SizeN, :),...
                        'verticalalignment','top','horizontalalignment','left','fontsize',8);
                    text(bboxFinCoord(mm,1),bboxFinCoord(mm,2)+20,sprintf('%0.2f', confCoord(mm)),'color','k','backgroundcolor',[1 0 0],...
                        'verticalalignment','top','horizontalalignment','left','fontsize',8);
                end
            end
        end
        
        if plotEn
            pause(2);
        end
    end
    
    fclose(fid);
    [recall,prec,ap]=VOCevaldet(VOCopts,'comp3upal',cls,true);  % compute and display PR
    mean(prec)
    mean(recall)
    if k<VOCopts.nclasses
        fprintf('press any key to continue with next class...\n');
        drawnow;
        pause;
    end
end



